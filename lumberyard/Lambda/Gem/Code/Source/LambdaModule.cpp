
#include <AzCore/Memory/SystemAllocator.h>
#include <AzCore/Module/Module.h>

#include <LambdaSystemComponent.h>

namespace Lambda
{
    class LambdaModule
        : public AZ::Module
    {
    public:
        AZ_RTTI(LambdaModule, "{1644F00F-50E3-4574-8DBA-23031B79FF81}", AZ::Module);
        AZ_CLASS_ALLOCATOR(LambdaModule, AZ::SystemAllocator, 0);

        LambdaModule()
            : AZ::Module()
        {
            // Push results of [MyComponent]::CreateDescriptor() into m_descriptors here.
            m_descriptors.insert(m_descriptors.end(), {
                LambdaSystemComponent::CreateDescriptor(),
            });
        }

        /**
         * Add required SystemComponents to the SystemEntity.
         */
        AZ::ComponentTypeList GetRequiredSystemComponents() const override
        {
            return AZ::ComponentTypeList {
                azrtti_typeid<LambdaSystemComponent>(),
            };
        }
    };
}

// DO NOT MODIFY THIS LINE UNLESS YOU RENAME THE GEM
// The first parameter should be GemName_GemIdLower
// The second should be the fully qualified name of the class above
AZ_DECLARE_MODULE_CLASS(Lambda_71203b0a5d5b4e368784b9f4a8bd146b, Lambda::LambdaModule)
