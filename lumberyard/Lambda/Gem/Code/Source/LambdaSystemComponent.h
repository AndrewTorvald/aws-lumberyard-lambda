
#pragma once

#include <AzCore/Component/Component.h>

#include <Lambda/LambdaBus.h>

namespace Lambda
{
    class LambdaSystemComponent
        : public AZ::Component
        , protected LambdaRequestBus::Handler
    {
    public:
        AZ_COMPONENT(LambdaSystemComponent, "{94920447-FEC0-4795-ABC2-298F17F19DF5}");

        static void Reflect(AZ::ReflectContext* context);

        static void GetProvidedServices(AZ::ComponentDescriptor::DependencyArrayType& provided);
        static void GetIncompatibleServices(AZ::ComponentDescriptor::DependencyArrayType& incompatible);
        static void GetRequiredServices(AZ::ComponentDescriptor::DependencyArrayType& required);
        static void GetDependentServices(AZ::ComponentDescriptor::DependencyArrayType& dependent);

    protected:
        ////////////////////////////////////////////////////////////////////////
        // LambdaRequestBus interface implementation

        ////////////////////////////////////////////////////////////////////////

        ////////////////////////////////////////////////////////////////////////
        // AZ::Component interface implementation
        void Init() override;
        void Activate() override;
        void Deactivate() override;
        ////////////////////////////////////////////////////////////////////////
    };

} // namespace Lambda
